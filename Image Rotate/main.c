#include <stdbool.h>
#include <stdio.h>
#include "utils/include/util.h"
#include "image/include/image.h"
#include "utils/include/fileOpenClose.h"
#include "bmp/include/bmpRW.h"
#include "transformation/include/rotation.h"

void usage() {
    fprintf(stderr, "Usage: ./image_rotate BMP_FILE_NAME OUTPUT_BMP_FILE_NAME\n");
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    if (argc > 3) err("Too many arguments \n" );


    FILE* input_file;
    FILE* output_file;

    file_open_check(open_file(&input_file, argv[1], "rb"));
    file_open_check(open_file(&output_file, argv[2], "wb"));

    struct image img = {0};

    bmp_read_check(from_bmp(input_file, &img));
    struct image new_img = rotate90deg(img);
    bmp_write_check(to_bmp(output_file, &new_img));

    file_close_check(close_file(&input_file));
    file_close_check(close_file(&output_file));

    return 0;

}
