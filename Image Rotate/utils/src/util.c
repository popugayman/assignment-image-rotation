#include "../include/util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

void file_open_check(enum open_status status)
{
    if (status==OPEN_OK) return;
    err("Error while opening file");
}

void file_close_check(enum close_status status)
{
    if (status==CLOSE_OK) return;
    err("Error while file closing");
}


static const char* bmp_read_status_messages[] = {
    [READ_INVALID_BITS] = "Invalid bits per pixel of file",
    [READ_INVALID_DATA_SIZE] = "Invalid pixel-data of file",
    [READ_INVALID_HEADER] = "Invalid header of file",
    [READ_INVALID_SIGNATURE] = "Invalid signature of file",
    [READ_OK] = ""
};

void bmp_read_check(enum read_status status)
{
    if (status==READ_OK) return;
    err(bmp_read_status_messages[status]);
}

static const char* bmp_write_status_messages[] = {
        [WRITE_HEADER_ERROR] = "Problem with header writing",
        [WRITE_DATA_ERROR] = "Promlen with pixels writing",
        [WRITE_OK] = ""
};


void bmp_write_check(enum write_status status)
{
    if (status==WRITE_OK) return;
    err(bmp_write_status_messages[status]);
}



