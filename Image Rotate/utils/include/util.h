#ifndef _UTIL_H_
#define _UTIL_H_
#include "fileOpenClose.h"
#include "../../bmp/include/bmpRW.h"
_Noreturn void err( const char* msg, ... );

void file_open_check(enum open_status status);
void file_close_check(enum close_status status);
void bmp_read_check(enum read_status status);
void bmp_write_check(enum write_status status);

#endif
